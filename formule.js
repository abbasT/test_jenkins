/*
JSONEditor.defaults.options.theme = 'bootstrap3';
JSONEditor.defaults.options.disable_edit_json= true;
JSONEditor.defaults.options.disable_properties= true;
if (jseditor instanceof window.JSONEditor) jseditor.destroy();
jseditor = new window.JSONEditor(document.querySelector("#json-editor-form"), jedata);
*/

jseditor = editor;
console.log(jseditor);

var dep_activated = true;
var stage = ['stage1', 'stage2', 'stage3', 'stage4', 'stage5', 'stage6', 'stage7', 'stage8'];
var brique = [
    ['stage1', 'oracle', 'ha_proxy', 'usw_prerequis_install', 'chocolatey'],
    ['stage2', 'jdk', 'oracle_client', 'apache_back', 'index_builder'],
    ['stage3', 'oracle_instant_client', 'passport', 'swym_cv', 'exalead_cv', 'interface', 'catia_batch'],
    ['stage4', 'federated', 'notification', 'dashboard', 'fcs_converter', 'apiwinn'],
    ['stage5', 'swym', 'comment', 'space', 'fcs', 'natives_apps'],
    ['stage6', 'pdfrenderingmonitor', 'space_index', 'fcs_config', 'studio', 'space_internal'],
    ['stage7', 'crawler_space_batch'],
    ['stage8', 'exporter_unix', 'exporter_windows']
];
var dependences = [
    ['passport', 'ha_proxy', 'oracle', 'usw_prerequis_install', 'jdk', 'oracle_client', 'apache_back'],
    ['swym_cv', 'usw_prerequis_install', 'jdk'],
    ['exalead_cv', 'usw_prerequis_install', 'usw_prerequis_install'],
    ['federated', 'passport'],
    ['notification', 'oracle_instant_client', 'passport'],
    ['dashboard', 'passport'],
    ['comment', 'passport'],
    ['swym', 'passport'],
    ['fcs_converter', 'usw_prerequis_install'],
    ['space', 'passport'],
    ['fcs', 'passport'],
    ['natives_apps', 'jdk'],
    ['pdfrenderingmonitor', 'space'],
    ['space_index', 'usw_prerequis_install'],
    ['fcs_config', 'space'],
    ['studio', 'usw_prerequis_install', 'jdk'],
    ['space_internal', 'space'],
    ['crawler_space_batch', 'space', 'space_index'],
    ['interface', 'usw_prerequis_install', 'jdk'],
    ['index_builder', 'space_internal'],
    ['catia_batch', 'chocolatey', 'jdk'],
    ['apiwinn', 'usw_prerequis_install']
];



var changeFunc = function (brique, stage, button) {
    var dependences_list = ['passport', 'swym_cv', 'exalead_cv', 'federated', 'notification', 'dashboard', 'fcs_converter', 'swym', 'comment', 'space', 'fcs', 'natives_apps', 'pdfrenderingmonitor', 'space_index', 'fcs_config', 'studio', 'space_internal', 'crawler_space_batch', 'index_builder', 'interface', 'catia_batch', 'apiwinn'];


    const contentinstallFull = jseditor.getEditor('root.general.installFull');
    var bool = true

    for (let j = 0; j < stage.length; j++) {
        for (let i = 1; i < brique[j].length; i++) {

            const content_to_modify = jseditor.getEditor('root.' + brique[j][0] + '.' + brique[j][i]);
            if (content_to_modify.getValue() === false) {
                contentinstallFull.setValue(false);

                bool = false;
            }
            else
                if (dependences_list.includes(brique[j][i]) && button === true) {
                    click_on_dependencies(brique[j][i]);
                }
                else
                    if (bool === true) {
                        contentinstallFull.setValue(true);

                    }
        }
    }

}

var collapse_function = function (content_installFull) {
    const value = jseditor.getValue();
    const content = value.general.installFull;

    var testElements = document.getElementsByClassName("btn btn-default json-editor-btn-collapse");
    var testElement_style = document.getElementsByClassName("well well-sm");

    for (let i = 2; i < testElements.length; i++) {
        var icon_elems;
        if (content === true) {
            icon_elems = document.getElementsByClassName("glyphicon glyphicon-chevron-down");
            icon_elems[2].setAttribute("class", "glyphicon glyphicon-chevron-right");
            testElement_style[i].setAttribute("style", "padding-bottom: 0px; display: none;");
            testElements[i].setAttribute("title", "Expand");
        } else {
            icon_elems = document.getElementsByClassName("glyphicon glyphicon-chevron-right");
            icon_elems[0].setAttribute("class", "glyphicon glyphicon-chevron-down");
            testElement_style[i].setAttribute("style", "padding-bottom: 0px;");
            testElements[i].setAttribute("title", "Collapse");
        }
    }
}

var clickfunction = function (brique, stage) {
    var installFull = "notclicked";
    var element = document.getElementsByName("root[general][installFull]")[0];
    element.addEventListener('click', function () {
        const value = jseditor.getValue();
        const content = value.general.installFull;

        for (let j = 0; j < stage.length; j++) {
            for (let i = 1; i < brique[j].length; i++) {
                const content_to_modify = jseditor.getEditor('root.' + brique[j][0] + '.' + brique[j][i]);
                if (content === false) {
                    content_to_modify.setValue(true);
                }
                else
                    if (content === true) {
                        content_to_modify.setValue(false);
                    }
            }
        }
    });

}.bind({});

const return_stage = function (brique_name) {

    for (let j = 0; j < stage.length; j++) {
        for (let i = 0; i < brique[j].length; i++) {
            if (brique_name == brique[j][i]) {
                stage_val = brique[j][0];
                break;
            }
        }
    }
    return stage_val;
}

var click_on_dependencies = function (brique_name, stage) {

    for (let j = 0; j < dependences.length; j++) {
        if (brique_name == dependences[j][0]) {
            for (let i = 1; i < dependences[j].length; i++) {
                var content = jseditor.getEditor('root.' + return_stage(dependences[j][i]) + '.' + dependences[j][i]);
                content.setValue(true);
            }
        }
    }
}

var main = function () {
    var dependencies_button = false;
    var button = jseditor.root.getButton('DEPENDANCES', '', 'DEPANDENCES');
    var button_holder = jseditor.root.theme.getHeaderButtonHolder();
    button_holder.appendChild(button);
    jseditor.root.header.parentNode.insertBefore(button_holder, jseditor.root.header.nextSibling);
    button.style.backgroundColor = "#d1462e";

    button.addEventListener('click', function () {

        if (dependencies_button === false) {
            dependencies_button = true;
            button.style.backgroundColor = "#74d48c";
        }
        else if (dependencies_button === true) {
            dependencies_button = false;
            button.style.backgroundColor = "#d1462e";
        }
    });

    jseditor.on('change', function () {
        changeFunc(brique, stage, dependencies_button);
        collapse_function();
    });

    clickfunction(brique, stage);

};

jseditor.on('ready', main);