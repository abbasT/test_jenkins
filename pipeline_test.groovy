import groovy.json.JsonSlurper

// import hudson.model.*
// import hudson.util.*

//def VarsTest = '{"general":{"installFull":true},"stage1":{"oracle":true,"ha_proxy":true,"usw_prerequis_install":false,"chocolatey":false},"stage2":{"jdk":true,"oracle_client":false,"apache_back":false,"index_builder":false}}'

// def cleanPrams(){
//     def thr = Thread.currentThread()
//     def build = thr?.executable
//     def parameters = build?.actions.find{ it instanceof ParametersAction }?.parameters
//     parameters.each {
//         println "parameter ${it.name}"
//         Thread.currentThread().executable.actions.remove(it)
//     }
// }

pipeline {
   agent any
   stages {
       stage('Install 3DX') {
           steps {
                script {
                    //print Vars
                    print "----"
                    def indx=0
                    def dict = []
                    def stg
                    def jsonSlurper = new JsonSlurper()
                    dict[indx]=[:]
                    dict[indx++]['base']=[]

                    Object input1 = jsonSlurper.parseText(Vars)
                    print input1.dependencies
                    //if(input1.dependencies.general.installFull)
                    input1.dependencies.Stages.each { key, value ->
                        stg = []
                        value.each { subkey, subvalue ->
                            if(subvalue){
                                stg.add(subkey)
                            }
                        }
                        dict[indx]=[:]
                        dict[indx++][key]=stg
                    }
                    //print dict
                    //print "----"
                    for (stage_dict in dict) {
                        for (stage_name in stage_dict.keySet()) {
                            for (int i = 0; i < stage_dict[stage_name].size(); i++) {
                                print stage_name+": installing brique "+stage_dict[stage_name][i]+" ..."
                            }
                        }
                    }

                }
           }
       }
   }

    // post {
    //     always {
    //         echo 'POST clean build infos'
    //         cleanPrams()
    //     }
    // }

}