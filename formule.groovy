// TODO: à utiliser avec include après tempalting jinja du job d'install (jenkins-jobs/resources)
version_usw_extendedchoice_store = 'master'
token_usw_extendedchoice_store = 'wjK8Ps5DTpLdh-fYyzvu'
def schema_json = [ "curl", "-s", "-H", "PRIVATE-TOKEN: ${token_usw_extendedchoice_store}", "https://si-devops-gitlab.edf.fr/api/v4/projects/22656/repository/files/schema%2Ejson/raw?ref=${version_usw_extendedchoice_store}" ].execute().text
def jsonEditorOptions = new groovy.json.JsonSlurper().parseText(/{
    "disable_edit_json": true,
    "disable_properties": true,
    "no_additional_properties": true,
    "disable_collapse": false,
    "disable_array_add": false,
    "disable_array_delete": false,
    "disable_array_reorder": false,
    "theme": "bootstrap3",
    "iconlib": "bootstrap3",
    "schema": ${schema_json}
}/);
return jsonEditorOptions;