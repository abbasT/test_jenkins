package structure.JobDSL
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
def branche_pipeline = ""

pipelineJob("JobDSL/folder1/job_test") {

    displayName("test job 1")
    description("Job to test plugin extended choice")

     configure { project ->
        project / 'properties' << 'hudson.model.ParametersDefinitionProperty' {
        parameterDefinitions {
            'com.cwctravel.hudson.plugins.extended__choice__parameter.ExtendedChoiceParameterDefinition' {
                name 'Vars'
                type 'PT_JSON'
                bindings 'version_3dx_automation = story/DUSWSYSTEAMCD-2002\ncred_id_token = TOKEN_3DX_AUTOMATION\npid = 5621'
                groovyScript '''
import com.cloudbees.plugins.credentials.Credentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.CredentialsMatcher
import com.cloudbees.plugins.credentials.CredentialsMatchers
import jenkins.model.*

def token_3dx_automation = ''
def getToken = { id ->
	Set<Credentials> allCredentials = new HashSet<Credentials>();
	def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
        com.cloudbees.plugins.credentials.Credentials.class
	);
	Jenkins.instance.getAllItems(com.cloudbees.hudson.plugins.folder.Folder.class).each{ elt ->
		creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(com.cloudbees.plugins.credentials.Credentials.class, elt)
		allCredentials.addAll(creds)
	}
	def c = allCredentials.findResult { it.id == id ? it : null }
	if ( c ) {
		println "found credential token ${c.id}"
		token_3dx_automation = c.secret
    } else {
		println "could not find credential token ${c.id}"
    }
}
getToken(cred_id_token)
def schema_json = [ "curl", "-s", "-H", "PRIVATE-TOKEN: ${token_3dx_automation}", "https://si-devops-gitlab.edf.fr/api/v4/projects/${pid}/repository/files/execution%2Fschema%2Ejson/raw?ref=${version_3dx_automation}" ].execute().text
def editor = new groovy.json.JsonSlurper().parseText(/{
    "disable_edit_json": true,
    "disable_properties": true,
    "no_additional_properties": true,
    "disable_collapse": false,
    "disable_array_add": false,
    "disable_array_delete": false,
    "disable_array_reorder": false,
    "theme": "bootstrap3",
    "iconlib": "bootstrap3",
    "schema": ${schema_json}
}/);
return editor;
'''
                javascript '''
jseditor = editor;
// console.log(jseditor);
// added : root.dependencies. ...
const contentinstallFull = jseditor.getEditor('root.dependencies.general.installFull');
const content = contentinstallFull.getValue();
const colorBtnDepActivated = "#74d48c";
const colorBtnDepNotActivated = "#d1462e";
const jseditorValue = jseditor.getValue().dependencies;
var brique = {
  "stage1": ["oracle", "ha_proxy", "usw_prerequis_install", "chocolatey"],
  "stage2": ["jdk", "oracle_client", "apache_back", "index_builder"],
  "stage3": ["oracle_instant_client", "passport", "swym_cv", "exalead_cv", "interface", "catia_batch"],
  "stage4": ["federated", "notification", "dashboard", "fcs_converter", "apiwinn"],
  "stage5": ["swym", "comment", "space", "fcs", "natives_apps"],
  "stage6": ["pdfrenderingmonitor", "space_index", "fcs_config", "studio", "space_internal"],
  "stage7": ["crawler_space_batch"],
  "stage8": ["exporter_unix", "exporter_windows"]
};
var dependences = {
  'swym_cv': ['usw_prerequis_install', 'jdk'],
  'exalead_cv': ['usw_prerequis_install', 'jdk'],
  'federated': ['passport'],
  'notification': ['oracle_instant_client', 'passport'],
  'dashboard': ['passport'],
  'comment': ['passport'],
  'swym': ['passport'],
  'fcs_converter': ['usw_prerequis_install'],
  'space': ['passport'],
  'passport': ['ha_proxy', 'oracle', 'usw_prerequis_install', 'jdk', 'oracle_client', 'apache_back'],
  'fcs': ['passport'],
  'natives_apps': ['jdk'],
  'pdfrenderingmonitor': ['space'],
  'space_index': ['usw_prerequis_install'],
  'fcs_config': ['space'],
  'studio': ['usw_prerequis_install', 'jdk'],
  'space_internal': ['space'],
  'crawler_space_batch': ['space', 'space_index'],
  'interface': ['usw_prerequis_install', 'jdk'],
  'index_builder': ['space_internal'],
  'catia_batch': ['chocolatey', 'jdk'],
  'apiwinn': ['usw_prerequis_install']
};

var onChangeMethod = function (button) {
  var bool = true;
  for (var key in brique) {
    for (let i = 0; i < brique[key].length; i++) {
      const content_to_modify = jseditor.getEditor('root.dependencies.Stages.' + key + '.' + brique[key][i]);
      if (content_to_modify.getValue() === false) {
        bool = false;
      }
      else
        if ((brique[key][i] in dependences) && button === true) {
          clickOnDependenciesMethod(brique[key][i]);
        }
    }
  }
  return bool;
}

var clickOnDependenciesMethod = function (brique_name) {
  var longueur = dependences[brique_name].length;
  for (let i = 0; i < longueur; i++) {
    var content = jseditor.getEditor('root.dependencies.Stages.' + returnStageMethod(dependences[brique_name][i]) + '.' + dependences[brique_name][i]);
    if (content.getValue() === false) {
      content.setValue(true);
      jseditorValue['Stages'][returnStageMethod(dependences[brique_name][i])][dependences[brique_name][i]] = true;
    }
    if ((dependences[brique_name][longueur - 1] in dependences)) {
      clickOnDependenciesMethod(dependences[brique_name][longueur - 1]);
    }
  }
}

var returnStageMethod = function (brique_name) {
  for (var key in brique) {
    for (let i = 0; i < brique[key].length; i++) {
      if (brique_name == brique[key][i]) {
        stage_val = key;
        break;
      }
    }
  }
  return stage_val;
}

var collapseButtonMethod = function () {
  const content = contentinstallFull.getValue();
  for (var key in jseditor.editors) {
    if (jseditor.editors.hasOwnProperty(key)) {
      var ed = jseditor.editors[key];
      if (['array', 'object'].indexOf(ed.schema.type) !== -1 && ed.editor_holder) {
      
      // added
      if (ed.schema.title.includes("Paramètres")) {
          ed.editor_holder.style.display = 'none';
          ed.collapsed = true;
          var iconElemsDown = document.getElementsByClassName("glyphicon glyphicon-chevron-down");
          if (iconElemsDown.length > 0) {
            iconElemsDown[iconElemsDown.length - 1].setAttribute("class", "glyphicon glyphicon-chevron-right");
          }
        }
        
        if (content === false && ed.schema.title.includes("Stage")) {
          ed.editor_holder.style.display = '';
          ed.collapsed = false;
          var iconElemsRight = document.getElementsByClassName("glyphicon glyphicon-chevron-right");
          if (iconElemsRight.length > 0) { iconElemsRight[iconElemsRight.length - 1].setAttribute("class", "glyphicon glyphicon-chevron-down"); }
        }
        else if (content === true && ed.schema.title.includes("Stage")) {
          ed.editor_holder.style.display = 'none';
          ed.collapsed = true;
          var iconElemsDown = document.getElementsByClassName("glyphicon glyphicon-chevron-down");
          if (iconElemsDown.length > 0) {
            iconElemsDown[iconElemsDown.length - 1].setAttribute("class", "glyphicon glyphicon-chevron-right");
          }
        }
      }
    }
  }
}

var clickInstallFullMethod = function () {
  var element = document.getElementsByName("root[dependencies][general][installFull]")[0];
  element.addEventListener('click', function () {
    var content = contentinstallFull.getValue();
    for (var key in brique) {
      for (let i = 0; i < brique[key].length; i++) {
        const content_to_modify = jseditor.getEditor('root.dependencies.Stages.' + key + '.' + brique[key][i]);
        if (content === false) {
          content_to_modify.setValue(true);
          jseditorValue['Stages'][key][brique[key][i]] = true;
        }
        else
          if (content === true) {
            content_to_modify.setValue(false);
            jseditorValue['Stages'][key][brique[key][i]] = false;
          }
      }
    }
  });
}

var styleCSSMethod = function () {

const obj = document.getElementsByClassName("well-sm")
for ( let i =3; i<10; i++){
  obj[i].firstChild.firstChild.setAttribute("style","display:flex");
}
  var styleEl = document.createElement('style');
  styleEl.innerHTML = `
  .row {
    flex-grow: 1;
  }
    input[type=checkbox]
    {
  display: inline;
  width: auto;
  margin-right: 7px;
    -ms-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    -o-transform: scale(1.5);
    padding: 5px;

    }

    input[type='checkbox'], input[type='radio'] { filter: grayscale(1); }
    .json-editor-btn-{
      color: #fff;
    }
    .json-editor-btn-:hover{
      color: #fff;
    }
    .json-editor-btn-:active{
      color: #fff;
    }
    .json-editor-btn-:focus{
      color: #fff;
    }
    .json-editor-btn-collapse {

      background-color:#cfc8c8;
    }
     .json-editor-btn-collapse:hover {
      background-color:#cfc8c8;
    }
     .json-editor-btn-collapse:active {

      background-color:#cfc8c8;
    }
     .json-editor-btn-collapse:focus {

      background-color:#cfc8c8;
    }
    `;
  document.head.appendChild(styleEl);
}

var main = function () {
  // default value
  contentinstallFull.setValue(true);
  jseditorValue.general.installFull = true;
  console.log(contentinstallFull.getValue())
  var dependencies_button = false;
  var button = jseditor.root.getButton('DEPENDANCES', '', 'DEPENDANCES');
  var button_holder = jseditor.root.theme.getHeaderButtonHolder();
  button_holder.appendChild(button);
  jseditor.root.header.parentNode.insertBefore(button_holder, jseditor.root.header.nextSibling);
  button.style.backgroundColor = colorBtnDepNotActivated;
  button.addEventListener('click', function () {
    if (dependencies_button === false) {
      dependencies_button = true;
      button.style.backgroundColor = colorBtnDepActivated;
    }
    else if (dependencies_button === true) {
      dependencies_button = false;
      button.style.backgroundColor = colorBtnDepNotActivated;
    }
  });
  jseditor.on('change', function () {
    var bool = onChangeMethod(dependencies_button);
    if (bool === true) {
      contentinstallFull.setValue(true);
    }
    else {
      contentinstallFull.setValue(false);
    }
    collapseButtonMethod();
  });
  styleCSSMethod();
  clickInstallFullMethod();
};
jseditor.on('ready', main);
                '''
            }
         }
       }
    }

    parameters {
        stringParam("Version", "develop", "Numéro de version applicative à déployer.\n"
                + "cf description du paramètre \"Emplacement Playbook\" pour plus d'information")
        choiceParam("Emp_Playbook", ['heads', 'tags'], "Emplacement du playbook correspondant au paramètre version.\n"
                + "Lorsque \"tag\" est selectionné, la variable version correspond à la version du tag à utiliser\n"
                + "Lorsque \"heads\" est selectionné, la variable version correspond au nom de la branche à utiliser")
        nonStoredPasswordParam("VAULT_PASSWORD", "")
        nonStoredPasswordParam("UNIX_SSH_PWD", "Mot de passe SSH de connexion aux machines UNIX cibles (CACCIA, ...)")
        nonStoredPasswordParam("UNIX_SUDO_PWD", "Mot de passe SUDO sur les machines UNIX cibles (laisser à blanc si sudoer nopasswd)")
        nonStoredPasswordParam("WIN_PWD", "Mot de passe de connexion aux machines WIN cibles (ADAM, ...)")
    }

    definition {
        cps { 
            script('''
Object input = jsonSlurper.parseText(Vars)
branche_pipeline = "refs/"+${input.parametres.emplacement_playbook}+"/"+${input.parametres.version}
            ''')
        }
        cpsScm {
            scm {
                git {
                    remote {
                        url("https://gitlab.com/abbasT/test_jenkins.git")
                    }
                    branch("${branche_pipeline}")
                }
            }
            scriptPath("pipeline_test.groovy")
        }
    }

}